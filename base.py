from .array import MeshDtype, MeshArray

def is_mesh_type(data):
    if isinstance(getattr(data, "dtype", None), MeshDtype):
        return True
    else:
        return False

def _delegate_property(op, this):
    a_this = MeshArray(this.geometry.values)
    data = getattr(a_this, op)
    if isinstance(data, GeometryArray):
        from .meshseries import MeshSeries

        return MeshSeries(data, index=this.index)
    else:
        return Series(data, index=this.index)