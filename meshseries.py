import pandas as pd
from pandas.core.internals import SingleBlockManager
from .array import MeshDtype
from trimesh import Trimesh
from .base import is_mesh_type
import warnings

def _meshseries_constructor_with_fallback(
    data=None, index=None, **kwargs
):
    try:
        return MeshSeries(data=data, index=index, **kwargs)
    except TypeError:
        return Series(data=data, index=index, **kwargs)

class MeshSeries(pd.Series):
    def __init__(self, data=None, index=None, **kwargs):
        if isinstance(data, SingleBlockManager):
            if not isinstance(data.blocks[0].dtype, MeshDtype):
                raise TypeError(
                    "Non mesh data passed to MeshSeries constructor, "
                    f"received data of dtype '{data.blocks[0].dtype}'"
                )

        if isinstance(data, Trimesh):
            n = len(index) if index is not None else 1
            data = [data] * n

        name = kwargs.pop("name", None)

        if not is_mesh_type(data):
            kwargs.pop("dtype", None)
            with warnings.catch_warnings():
                empty_msg = "The default dtype for empty Series"
                warnings.filterwarnings("ignore", empty_msg, DeprecationWarning)
                warnings.filterwarnings("ignore", empty_msg, FutureWarning)
                s = pd.Series(data, index=index, name=name, **kwargs)
            if s.dtype != object:
                if (s.empty and s.dtype == "float64") or data is None:
                    s = s.astype(object)
                else:
                    raise TypeError(
                        "Non mesh data passed to MeshSeries constructor, "
                        f"received data of dtype '{s.dtype}'"
                    )
            index = s.index
            name = s.name

        super().__init__(data, index=index, name=name, **kwargs)

    def append(self, *args, **kwargs) -> MeshSeries:
        return self._wrapped_pandas_method("append", *args, **kwargs)

    def x(self) -> Series:
        return _delegate_property("x", self)

    def y(self) -> Series:
        return _delegate_property("y", self)

    def z(self) -> Series:
        return _delegate_property("z", self)

    @classmethod
    def from_xyz(cls, x, y, z, index=None, **kwargs) -> MeshSeries:
        if index is None:
            if (
                isinstance(x, Series)
                and isinstance(y, Series)
                and isinstance(z, Series) 
                and x.index.equals(y.index)
                and x.index.equals(z.index)))
            ):
                index = x.index
        return cls(points_from_xyz(x, y, z), index=index, **kwargs)

        @property
    def _constructor(self):
        return _meshseries_constructor_with_fallback

    def _wrapped_pandas_method(self, mtd, *args, **kwargs):
        val = getattr(super(), mtd)(*args, **kwargs)
        if type(val) == Series:
            val.__class__ = MeshSeries
            return val
    
            def __getitem__(self, key):
        return self._wrapped_pandas_method("__getitem__", key)

    @doc(pd.Series)
    def sort_index(self, *args, **kwargs):
        return self._wrapped_pandas_method("sort_index", *args, **kwargs)

    @doc(pd.Series)
    def take(self, *args, **kwargs):
        return self._wrapped_pandas_method("take", *args, **kwargs)

    @doc(pd.Series)
    def select(self, *args, **kwargs):
        return self._wrapped_pandas_method("select", *args, **kwargs)

    @doc(pd.Series)
    def apply(self, func, convert_dtype: Optional[bool] = None, args=(), **kwargs):
        if convert_dtype is not None:
            kwargs["convert_dtype"] = convert_dtype
        else:
            # if compat.PANDAS_GE_21 don't pass through, use pandas default
            # of true to avoid internally triggering the pandas warning
            if not compat.PANDAS_GE_21:
                kwargs["convert_dtype"] = True

        # to avoid warning
        result = super().apply(func, args=args, **kwargs)
        return result

    def isna(self) -> Series:
        return super().isna()

    def isnull(self) -> Series:
        return self.isna()

    def notna(self) -> Series:
        return super().notna()

    def notnull(self) -> Series:
        return self.notna()

