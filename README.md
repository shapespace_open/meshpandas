MeshPandas
---------

Python tools for 3D mesh data

Note
----

MeshPandas is still in development. This readme describes what its functionality will be when it is complete.

Introduction
------------

MeshPandas is a project to add support for 3D mesh data to [pandas] (http://pandas.pydata.org) objects.  It currently implements `MeshSeries` and `MeshDataFrame` types which are subclasses of `pandas.Series` and `pandas.DataFrame` respectively.  MeshPandas objects can act on [Trimesh](https://trimesh.org/) objects and perform geometric operations.

Documentation will be available in due course.

Install
--------

Installation instructions will be provided when MeshPandas is complete. It will depend on the following packages:

- ``pandas``
- ``trimesh``
- ``pyogrio``
- ``packaging``

At this early stage in the development process, we cannot guarantee that these are the only packages MeshPandas will depend on.

Get in touch
------------

- Report bugs, suggest features or view the source code [on GitLab](https://gitlab.com/shapespace_open/meshpandas).
- For any other questions, feel free to [email me](mailto:joey@shapespace.com).

Functionality
-------------

The methods and attributes we currently plan  to have available on MeshSeries objects are:

### ``MeshSeries.bounds()``

Returns a DataFrame with columns ``minx``, ``miny``, ``minz``, ``maxx``, ``maxy`` and ``maxz`` containing the bounds for each mesh.

### ``MeshSeries.distance(other)``

Returns a Series containing the minimum distance to the other MeshSeries (elementwise) or geometric object.

### ``MeshSeries.length()``

Returns a Series containing the length of each mesh.

### ``MeshSeries.representative_point()``

Returns a MeshSeries of (cheaply computed) points that are guaranteed to be within each mesh.

### ``MeshSeries.surface_area()``

Returns a Series containing the surface area of each mesh in the MeshSeries.

### ``MeshSeries.volume()``

Returns a Series containing the volume of each mesh in the MeshSeries.

### ``MeshSeries.is_closed()``

Returns a Series of ``dtype('bool')`` with value ``True`` for closed meshes.

### ``MeshSeries.is_empty()``

Returns a Series of ``dtype('bool')`` with value ``True`` for empty meshes.

### ``MeshSeries.almost_equals(other[, decimal=6])``

Returns a Series of ``dtype('bool')`` with value ``True`` if each object is approximately equal to the other at all points to specified decimal place precision.

### ``MeshSeries.contains(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if each object’s interior contains the boundary and interior of the other object and their boundaries do not touch at all.

### ``MeshSeries.crosses(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if the interior of each object intersects the interior of the other but does not contain it, and the dimension of the intersection is less than the dimension of the one or the other.

### ``MeshSeries.disjoint(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if the boundary and interior of each object does not intersect at all with those of the other.

### ``MeshSeries.equals(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if the set-theoretic boundary, interior, and exterior of each object coincides with those of the other.

### ``MeshSeries.intersects(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if the boundary and interior of each object intersects in any way with those of the other.

### ``MeshSeries.touches(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if the objects have at least one point in common and their interiors do not intersect with any part of the other.

### ``MeshSeries.within(other)``

Returns a Series of ``dtype('bool')`` with value ``True`` if each object’s boundary and interior intersect only with the interior of the other (not its boundary or exterior).

### ``MeshSeries.boundary()``

Returns a MeshSeries of lower dimensional objects representing each mesh's set-theoretic boundary.

### ``MeshSeries.centroid()``

Returns a MeshSeries of points for each geometric centroid.

### ``MeshSeries.difference(other)``

Returns a MeshSeries of the points in each mesh that are not in the other object.

### ``MeshSeries.intersection(other)``

Returns a MeshSeries of the intersection of each object with the other geometric object.

### ``MeshSeries.symmetric_difference(other)``

Returns a MeshSeries of the points in each object not in the other geometric object, and the points in the other not in this object.

### ``MeshSeries.union(other)``

Returns a MeshSeries of the union of points from each object and the other geometric object.

### ``MeshSeries.rotatex(self, angle, origin='center', use_radians=False)``

Rotate the coordinates of the MeshSeries about the X-axis.

### ``MeshSeries.rotatey(self, angle, origin='center', use_radians=False)``

Rotate the coordinates of the MeshSeries about the Y-axis.

### ``MeshSeries.rotatez(self, angle, origin='center', use_radians=False)``

Rotate the coordinates of the MeshSeries about the Z-axis.

### ``MeshSeries.scale(self, xfact=1.0, yfact=1.0, zfact=1.0, origin='center')``

Scale the geometries of the MeshSeries along each (x, y, z) dimension.

### ``MeshSeries.skewxy(self, angle, origin='center', use_radians=False)``

Shear/Skew the geometries of the MeshSeries by angles along x and y dimensions.

### ``MeshSeries.skewyz(self, angle, origin='center', use_radians=False)``

Shear/Skew the geometries of the MeshSeries by angles along y and z dimensions.

### ``MeshSeries.skewxz(self, angle, origin='center', use_radians=False)``

Shear/Skew the geometries of the MeshSeries by angles along x and z dimensions.

### ``MeshSeries.translate(self, angle, origin='center', use_radians=False)``

Shift the coordinates of the MeshSeries.

### ``MeshSeries.unary_union()``

Returns a mesh containing the union of all meshes in the MeshSeries.

### ``MeshSeries.from_file()``

Load a MeshSeries from a CSV file.

### ``MeshSeries.to_file(filename, **kwargs)``

Write a MeshSeries to a CSV file.

### ``MeshSeries.plot()``

Generates a 3D rendering of the meshes in the MeshSeries. 

### ``MeshSeries.total_bounds()``

Returns a tuple containing ``minx``, ``miny``, ``minz``, ``maxx``, ``maxy``, ``maxz`` values for the bounds of the series as a whole.

The methods and attributes we currently plan to have available on MeshDataFrame objects are:

### ``@classmethod MeshDataFrame.from_file(filename, **kwargs)``

Load a MeshDataFrame from a CSV file.

### ``MeshDataFrame.to_file(filename, **kwargs)``

Write a MeshDataFrame to a CSV file.