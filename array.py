import pandas as pd
import numpy as np
from trimesh import Trimesh

class MeshDtype(pd.api.extensions.ExtensionDtype):
    type = Trimesh
    name = "mesh"
    na_value = np.nan

    @classmethod
    def construct_from_string(cls, string):
        if not isinstance(string, str):
            raise TypeError(
                "'construct_from_string' expects a string, got {}".format(type(string))
            )
        elif string == cls.name:
            return cls()
        else:
            raise TypeError(
                "Cannot construct a '{}' from '{}'".format(cls.__name__, string)
            )

    @classmethod
    def construct_array_type(cls):
        return MeshArray

class MeshArray(pd.api.extensions.ExtensionArray):
    """
    Class wrapping a numpy array of Shapely objects and
    holding the array-based implementations.
    """

    _dtype = MeshDtype()

    def __init__(self, data):
        if isinstance(data, self.__class__):
            data = data._data
        elif not isinstance(data, np.ndarray):
            raise TypeError(
                "'data' should be array of mesh objects. Use from_shapely, "
                "from_wkb, from_wkt functions to construct a MeshArray."
            )
        elif not data.ndim == 1:
            raise ValueError(
                "'data' should be a 1-dimensional array of mesh objects."
            )
        self._data = data
        self._sindex = None

def points_from_xyz(x, y, z):
    x = np.asarray(x, dtype="float64")
    y = np.asarray(y, dtype="float64")
    z = np.asarray(z, dtype="float64")

    return MeshArray(None)
    #None is placeholder until I can work out the Trimesh equivalent of shapely.points()
